<?php

/**
 * Tokenizer Tokenize Example
 * 
 * This script verifies the token of a user that returns to your application.
 *
 * @author Frank Broersen <frank@amsterdamstandard.com>
 * @copyright Amsterdam Standard
 * @homepage http://amsterdamstandard.com
 */

include 'config.php';

try {
    
    $id = isset($_GET['id']) ? $_GET['id'] : false;
    if(!$id) {
        throw new Tokenizer\Exception("No url id found");
    }
    
    $tokenizer = new Tokenizer([
        'app_id'  => APP_ID,
        'app_key' => APP_KEY,
    ]);
    
    if($tokenizer->getSession('tokenizer_id') === null) {
        throw new Tokenizer\Exception("No tokenizer session found");
    }
    
    if($tokenizer->getSession('tokenizer_id') != $id) {
        throw new Tokenizer\Exception("Tokenizer session does not match url id");
    }
    
    if( $tokenizer->verifyAuth($id) ) {
        echo 'Your authentication has been accepted';
    } else {
        echo 'Your authentication has not (yet) been accepted';
    } 
    
} catch(Tokenizer\Exception $e) {
    echo $e->getMessage();
    exit;
}