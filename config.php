<?php

// start session
session_start();

// include tokenizer classes
include 'classes/Tokenizer.php';
include 'classes/Tokenizer/Exception.php';
include 'classes/Tokenizer/Connector.php';
include 'classes/Tokenizer/Response.php';
include 'classes/Tokenizer/Response/Create.php';
include 'classes/Tokenizer/Response/Verify.php';
include 'classes/Tokenizer/Response/Config.php';

// app info
define('APP_ID',    false);
define('APP_KEY',   false);

// the path to your tokenize script
define('APP_URL',  'http://path/to/tokenize.php?id=');